import org.scalatest.funsuite.AnyFunSuite
import Point.*
import Player.*
import Score.*

class ScoreTest extends AnyFunSuite:

  private val player1: Player1 = Player1("pippo")
  private val player2: Player2 = Player2("pluto")

  test("player 1 score") {
    val score = Score.scoreFor(player1, Points(Love, Love))
    assert(score == Points(Fifteen, Love))
  }

  test("player 1 wins") {
    val score = Score.scoreFor(player1, Points(Forty, Thirty))
    assert(score == Win(player1))
  }

  test("player 1 advantage") {
    val score = Score.scoreFor(player1, Deuce)
    assert(score == Advantage(player1))
  }

  test("player 2 deuce") {
    val score = Score.scoreFor(player2, Points(Forty, Thirty))
    assert(score == Deuce)
  }

  test("player 1 wins for advantage") {
    val score = Score.scoreFor(player1, Advantage(player1))
    assert(score == Win(player1))
  }

  test("player 2 break the advantage") {
    val score = Score.scoreFor(player2, Advantage(player1))
    assert(score == Deuce)
  }
