import Point.*
import Player.*

enum Score:
  case Points(player1: Point, player2: Point)
  case Deuce
  case Advantage(player: Player)
  case Win(player: Player)

object Score:
  private def points(scorer: Player, player1: Point, player2: Point): Score =
    (scorer, player1, player2) match
      case (Player1(_), Thirty, Forty) => Deuce
      case (Player2(_), Forty, Thirty) => Deuce
      case (Player1(_), Forty, _)      => Win(scorer)
      case (Player2(_), _, Forty)      => Win(scorer)
      case (Player1(_), x, y)          => Points(Point.next(x), y)
      case (Player2(_), x, y)          => Points(x, Point.next(y))

  def scoreFor(player: Player, score: Score): Score =
    score match
      case Points(Forty, Forty) =>
        throw new MatchError("next of Points(Forty, Forty) doesn't exist")
      case Win(_) => throw new MatchError("next of Win doesn't exist")
      case Points(player1, player2)    => points(player, player1, player2)
      case Deuce                       => Advantage(player)
      case Advantage(p) if p == player => Win(player)
      case Advantage(_)                => Deuce
