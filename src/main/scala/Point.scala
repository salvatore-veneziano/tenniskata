enum Point:
  case Love
  case Fifteen
  case Thirty
  case Forty

object Point:
  def next(x: Point): Point = x match
    case Love    => Fifteen
    case Fifteen => Thirty
    case Thirty  => Forty
    case Forty   => throw new MatchError("next of Forty doesn't exist")
